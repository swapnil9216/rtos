
#ifndef __USB_TASK_H
#define __USB_TASK_H

//***** include parameter **************************************************************************************
#include "main.h"
#include "Consol_Task.h"
#include "rl_fs.h"
#include "rl_usb.h"

//*****define parameter*****************************************************************************************

#define USB_QUEUE_PAYLOAD     30

#define FILE_NAME_SIZE        10
#define FOLDER_NAME_SIZE      10

#define STRUCTURE_USB_SIZE   (FOLDER_NAME_SIZE + FILE_NAME_SIZE + USB_QUEUE_PAYLOAD + 1 )

#define MAX_USB_RETURN_BUFFER 10

//USB commond id 

#define USB_WRITE_DATA_COMMOND   		'1'
#define USB_MOUNT_COMMOND    				'2' 
#define USB_UNMOUNT_COMMOND 				'3' 
#define USB_APPEND_DATA_COMMOND  		'4'  
#define USB_CREATE_FILE_COMMOND  		'5' 
#define USB_FORMAT_COMMOND 					'6'
#define USB_BACKUP_COMMOND					'7'

//USB commond responce

#define USB_OPERATION_OK						48
#define USB_OPERATION_ERROR					49


//*******strucute/union***************************************************************************************

typedef struct 
{
	char usb_commond_id[1];
	char folder_name[FILE_NAME_SIZE];
  char file_name[FOLDER_NAME_SIZE];	
	char payload_usb[USB_QUEUE_PAYLOAD];
	
}USB_IN_STRUCT;

typedef struct
{
		char usb_commond_id;
		char operation_status;
	  char payload_usb_return[MAX_USB_RETURN_BUFFER];
	}USB_RETURN_STRUCT;


//******extern***********************************************************************************************

extern osThreadId_t tid_USB_Task;
extern osMessageQueueId_t Q_USB_Task_IN;                               // USB_Que_id

//******function declearation*********************************************************************************

void USB_Task (void *argument);
//void commond_process_usb (CONSOL_TASK_STRU_1*);
	void commond_process_usb (CONSOL_TASK_STRU_1*,USB_RETURN_STRUCT*);

#endif