
#ifndef __KEYPAD_H
#define __KEYPAD_H


// include parameter **************************************************************************************
//#include "Thread_param.h"
#include "main.h"
#include <stdlib.h>
//#include <RTL.h>

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Consol_Task.h"

//define parameter*****************************************************************************************
#define POLL_TIME             10
#define MAX_POLL_TIME					300


#define KEY_ROW0               0
#define KEY_ROW1               1
#define KEY_ROW2               2
#define KEY_ROW3               3

#define KEY_COL0               0
#define KEY_COL1               1
#define KEY_COL2               2
#define KEY_COL3               3

#define IO_ROW0                25   /*P3.25*/
#define IO_ROW1                26   /*P3.26*/
#define IO_ROW2                27   /*P3.27*/
#define IO_ROW3                28   /*P3.28*/

#define IO_COL0                21   /*P3.21*/
#define IO_COL1                22   /*P3.22*/
#define IO_COL2                23   /*P3.23*/
#define IO_COL3                24   /*P3.24*/

#define PIN_ROW0               P3_25   /*P3.25*/
#define PIN_ROW1               P3_26   /*P3.26*/
#define PIN_ROW2               P3_27   /*P3.27*/
#define PIN_ROW3               P3_28   /*P3.28*/

#define PIN_COL0               P3_21   /*P3.21*/
#define PIN_COL1               P3_22   /*P3.22*/
#define PIN_COL2               P3_23   /*P3.23*/
#define PIN_COL3               P3_24   /*P3.24*/

#define NO_KEY_PRESSED_CODE    0x00

#define ROW_PORT               LPC_GPIO3
#define COL_PORT               LPC_GPIO3

#define ROW_MASK               ((1<<IO_ROW0) | (1<<IO_ROW1) | (1<<IO_ROW2) | (1<<IO_ROW3))
#define COL_MASK               ((1<<IO_COL0) | (1<<IO_COL1) | (1<<IO_COL2) | (1<<IO_COL3))

#define KEYPAD_INT_PORT        P0_26
#define KEYPAD_INT_PIN         26
#define PORT_INT_STATUS        IO0IntStatR


//#define LOG_KEY_STATUS
//#define KEYPAD_INT_BASED
/*===========================================================================*/
/* Defines Section */
#define THIRTY_SEC_TICKS  3000
#define FIFTEEN_SEC_TICKS 1500



#define SET_BIT(REG, BIT)     ((REG) |= (0x01<< BIT))

#define READ_BIT(REG, BIT)    ((REG) & (0x01<< BIT))

#define RESET_BIT(REG, BIT)   ((REG) &= ~(BIT))

//#define RESET_BIT(REG, BIT)    ((REG) &= (0xFFFE << BIT))

#define CLEAR_REG(REG)        ((REG) = (0x0))


#define KEY0									0           // row0 , colum 0
#define KEY1									1           // row0 , colum 1
#define KEY2									2           // row0 , colum 2
#define KEY3									3           // row0 , colum 3
#define KEY4									4           // row1 , colum 0
#define KEY5									5           // row1 , colum 1
#define KEY6									6           // row1 , colum 2
#define KEY7									7           // row1 , colum 3
#define KEY8									8           // row2 , colum 0
#define KEY9									9           // row2 , colum 1
#define KEY10									10           // row2 , colum 2
#define KEY11									11          // row2 , colum 3
#define KEY12									12          // row3 , colum 0
#define KEY13									13          // row3 , colum 1
#define KEY14									14          // row3 , colum 2
#define KEY15									15          // row3 , colum 3

#define MAX_NO_KEYS   16
#define SET_KEY       0
#define RESET_KEY     1

#define FULL_SET_DEBOUNCE 				0xFF
#define FULL_RESET_DEBOUNCE 			0x00

#define INPUT_RESPONSE  '<'

#define OUTPUT_RESPONSE '>'

#define COMMOND_ID_1  '0'
#define COMMOND_ID_2  '2'



#define COMMOND_SOURCE_KEYPAD  'K'
#define COMMOND_SOURCE_UART    'U'

#define COMMOND_DIVIDE  '|'

#define END_CHAR_1  '\r'
#define END_CHAR_2  '\n'

#define END_CHAR_DATA   "\r\n"


#define PAYLOAD_BASE64    01
#define PAYLOAD_NONE      02

//strucute/union***************************************************************************************
	typedef struct 
		{                                                   // object data type
		uint16_t  KeypadState;
		uint16_t  Keypad_Direction;
		uint16_t  KeypadHoldTime;
		uint16_t  Dummy;
		
	} KEYPAD_TASK_STRU;
	
	
	
typedef union
{
		uint8_t		U8[4];
		uint16_t 	U16[2];
		uint32_t	U32;
}Data32Bit;

typedef union
{
		uint8_t		U8[2];
		uint16_t 	U16;
}Data16Bit;

// extern from function ***********************************************************************

extern osThreadId_t tid_Keypad_Task;
extern osMessageQueueId_t Q_Keypad_task_OUT; 
extern osMutexId_t mutex_is_WrapConsole_data;

//function declearation************************************************************************

void keypad_init (void);
uint16_t keypad_scan (void);
uint8_t Set_Debounce_Buff(uint8_t Debounce_Buffer , uint8_t stuff_bit);
void send_through_queue(KEYPAD_TASK_STRU keypad_stru);

char* itoa(int num, char* str, int base);

//CONSOL_TASK_STRU_1 WrapConsole_data(char* keypad_api_base64 );
//CONSOL_TASK_STRU_1 WrapConsole_data(KEYPAD_TASK_STRU keypad_stru_wrapper , WRAPPER_INPUT wrapper_data )	;




#endif  /* KEYPAD_H_ */

