#include "cmsis_os2.h"                                        // CMSIS RTOS header file
#include "Consol_Task.h"
#include "Keypad.h"

#include <ctype.h>
 
/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
uint16_t Keypad_Direction;
osThreadId_t tid_Keypad_Task;                                      // thread id
extern osThreadId_t tid_Consol_Task;
osMutexId_t mutex_is_WrapConsole_data;


/* .................................Queue ID...................................*/


//KEYPAD_TASK_STRU keypad_stru;
KEYPAD_TASK_STRU dummy_keypad_stru;

osMessageQueueId_t Q_Keypad_task_OUT; 

/*............................*/
//char *keypad_api_base64;
//char *keypad_api_base64;
//char keypad_api_base64[13];
 
int Init_Keypad_Task (void) 
	{
 
  osThreadAttr_t attr =
		{
			.name = "Consol_Task",
			.attr_bits = osThreadDetached,
			.cb_mem = NULL,
			.cb_size = 0,
			.stack_mem = NULL,
			.stack_size = 256,
			.priority = KEYPAD_PRIORITY,
		};
		
			
		tid_Keypad_Task = osThreadNew (Keypad_Task, NULL, &attr);

  
  return tid_Keypad_Task != NULL;
}
 
void Keypad_Task (void *argument) 
{
	keypad_init();
	uint16_t keypad_status ,keypad_status_old ;
	uint16_t KeypadHoldTime ;
	uint32_t KeypadHoldTime_init,KeypadHoldTime_current,KeypadHoldTime_init_old ,add_keypadholdtime =0;
	
	KEYPAD_TASK_STRU keypad_stru;
	CONSOL_TASK_STRU_1 s_console_in;
	WRAPPER_INPUT  wrapper_input_keypad;
	char buffer_data[8];
	//create mutex ..........................................
	const osMutexAttr_t Thread_Mutex_attr = {
			"Wrapping_console_mutex",     // human readable mutex name
			osMutexPrioInherit,  // attr_bits
			NULL,                // memory for control block   
			0U                   // size for control block
		};
	
	mutex_is_WrapConsole_data = osMutexNew(&Thread_Mutex_attr);
	
		
		

	
	//  Q_Keypad_task_OUT  =  osMessageQueueNew(KEYPAD_OUT_SIZE, sizeof(KEYPAD_TASK_STRU), NULL);  //In queue initilized 

 // Q_Keypad_task_OUT  =  osMessageQueueNew(10, 10, NULL);  //In queue initilized 
	
	wrapper_input_keypad.response_type = INPUT_RESPONSE;
	
	wrapper_input_keypad.cmd_hi  = COMMOND_CON_KEYPAD;
	wrapper_input_keypad.cmd_lo  = COMMOND_SOURCE_KEYPAD;
	
	wrapper_input_keypad.payload_type = PAYLOAD_BASE64;
	
	
	
	
	
	wrapper_input_keypad.payload_size = sizeof(KEYPAD_TASK_STRU);

  while (1) 
		{
			osDelay(POLL_TIME);
			
			keypad_status_old = keypad_status;																		// Saving keypad status as old keypad status
			keypad_status = keypad_scan() ;																				// check current keypad status 
			
			if (keypad_status_old != keypad_status)																//if we receive different keypad_status then send data
			{
				KeypadHoldTime_init = osKernelGetTickCount();												// get initial time for keypad
				KeypadHoldTime =0; 
				add_keypadholdtime = MAX_POLL_TIME; 																//use max_poll time to check contineous data post
				
				keypad_stru.KeypadState = keypad_status;														// save keypad status in structure
				keypad_stru.KeypadHoldTime = KeypadHoldTime;												// save keypad hold time 
				keypad_stru.Keypad_Direction = Keypad_Direction;										// save keypad direction 
				keypad_stru.Dummy  = NULL;
				
//				send_through_queue(keypad_stru);																								//send data to connsole task using queue 

				memcpy(buffer_data,&keypad_stru,sizeof(KEYPAD_TASK_STRU));
				
//				s_console_in =  WrapConsole_data(buffer_data,wrapper_input_keypad);			
				
				osMutexAcquire(mutex_is_WrapConsole_data, osWaitForever);
				WrapConsole_data(buffer_data,wrapper_input_keypad , &s_console_in);
				osMutexRelease(mutex_is_WrapConsole_data);
				osMessageQueuePut (Q_Consol_Task_IN ,&s_console_in ,0,NULL);
				
				
			}	
			Keypad_Direction =NULL;																								// set keypad direction to 0 
			
			if(keypad_status)																											// if keypad status is non zero checck hold time 
			{
			
					KeypadHoldTime_current = osKernelGetTickCount();											//get current tick time 			
					KeypadHoldTime = KeypadHoldTime_current - KeypadHoldTime_init;        // Need to handle overflow of count 
						
			}
						
			
			if(KeypadHoldTime > add_keypadholdtime && keypad_status /*&& (!(KeypadHoldTime_init_old == KeypadHoldTime_init))*/) // if hold time is more than max holsd time 
			{
				keypad_stru.KeypadState = keypad_status;																//save latest keypad status 
				keypad_stru.KeypadHoldTime = KeypadHoldTime;														// save latest hold time 
				keypad_stru.Keypad_Direction = Keypad_Direction;												// check latest hold time 
				
				KeypadHoldTime_init_old = KeypadHoldTime_init;													// change hold time to initial time 
				
//				osMessageQueuePut (Q_uart_send_thread_IN, &keypad_stru, 0, NULL);
//				osThreadFlagsSet  (tid_uart_send_thread, FLAG_CONSOLE_TASK);

//				send_through_queue(keypad_stru);																										// send data to console 
//				KeypadHoldTime = 0;																											// change hold time to zero 
				
				memcpy(&buffer_data,&keypad_stru,sizeof(keypad_stru));
//				s_console_in =  WrapConsole_data(buffer_data,wrapper_input_keypad);  		//wrapper_input_keypad data 
				
				
				WrapConsole_data(buffer_data,wrapper_input_keypad , &s_console_in);
				
				osMessageQueuePut (Q_Consol_Task_IN ,&s_console_in ,0,NULL); 			//Put data in console task queue
							
				add_keypadholdtime = add_keypadholdtime + MAX_POLL_TIME;					//set up next timing to send data 
								
			}			

		}
}


/*****************************************************************************
* @note       Function name: CONSOL_TASK_STRU_1 WrapConsole_data(char* keypad_stru_wrapper , WRAPPER_INPUT wrapper_data )	
* @returns    returns      : CONSOL_TASK_STRU_1 
* @param      arg1         : char* , WRAPPER_INPUT
* @author                  : Swapnil
* @date       date created : 2018-04
* @brief      Description  : This function is used to wrap data as per console task queue . It can be called through any task 
															whenever we want to send data to console task 
* @note       Notes        : We will move this function to seperate file 
*****************************************************************************/
	
//CONSOL_TASK_STRU_1* WrapConsole_data(char* payload_data_wrapper , WRAPPER_INPUT wrapper_data )	
	void WrapConsole_data(char* p_str_payload_data_wrapper ,WRAPPER_INPUT wrapper_data ,CONSOL_TASK_STRU_1* s_console_in_wrapper )	
{


//				CONSOL_TASK_STRU_1 s_console_in_wrapper; 
				char input_string[MAX_PAYLOAD_SIZE_CONSOLE_IN] ;
				char keypad_api_base64[13];
				

				Data32Bit keypadHoldTime_ch;
				
//				memset(s_console_in_wrapper->payload,'\0',wrapper_data.payload_size);

				s_console_in_wrapper->direction[0] = wrapper_data.response_type;                			// fist to show input commond 
				s_console_in_wrapper->direction[1] = wrapper_data.response_type;											// fist to show input commond 
				
				s_console_in_wrapper->Cmd_hi 					= wrapper_data.cmd_hi ;																	// cmd_Hi
				s_console_in_wrapper->Cmd_lo 					= wrapper_data.cmd_lo ;																	// cmd_low
				s_console_in_wrapper->ascii_seperator  = COMMOND_DIVIDE;

				switch(wrapper_data.payload_type)
				{
					
					case PAYLOAD_BASE64 :									//this function is used when we need to chane from HEX to base64 
					{
							memset(input_string,'\0',wrapper_data.payload_size);
							memcpy(input_string,p_str_payload_data_wrapper,wrapper_data.payload_size);

						
//							Base64_Encode_Internal(input_string,sizeof(input_string)); 
							Base64_Encode_Internal(input_string,wrapper_data.payload_size);
							
							strcpy(s_console_in_wrapper->payload,decoded_buf);				
//							memcpy(&dummy_keypad_stru,input_string,sizeof(keypad_stru));
							free(decoded_buf);	                                                             //free malloc used in base64
					
//							memcpy(s_console_in_wrapper.payload,&keypad_api_base64,sizeof(keypad_api_base64));
						
							strcat(s_console_in_wrapper->payload ,END_CHAR_DATA);
					
					}
					
					break;
					
					case PAYLOAD_NONE :        //this case is used when we dont need to change data type 
					{

//							memset(s_console_in_wrapper->payload,'\0',wrapper_data.payload_size);
							memcpy(s_console_in_wrapper->payload,p_str_payload_data_wrapper,wrapper_data.payload_size);
//							strcat(s_console_in_wrapper->payload ,END_CHAR_DATA);
					}
					
					
					
					
				};				
//		return &s_console_in_wrapper;
		
}





/*****************************************************************************
* @note       Function name: void keypad_init (void)
* @returns    returns      : None
* @param      arg1         : None
* @author                  : Swapnil
* @date       date created : 
* @brief      Description  : Initialize the scan and return lines of the keypad
* @note       Notes        : None
*****************************************************************************/
void keypad_init (void)
{
    //  U32 status;

    /*Initialize rows as scanlines(outputs)*/
    LPC_IOCON->PIN_ROW0 = 0x20;                      /* P3.25 is GPIO-KeyRow0*/
    LPC_IOCON->PIN_ROW1 = 0x20;                      /* P3.26 is GPIO-KeyRow1*/
    LPC_IOCON->PIN_ROW2 = 0x20;                      /* P3.27 is GPIO-KeyRow2*/
    LPC_IOCON->PIN_ROW3 = 0x20;                      /* P3.28 is GPIO-KeyRow3*/

    /* set the pins as outputs and set the mask register */
    ROW_PORT->DIR |= ROW_MASK;

    /* output 1 */
    ROW_PORT->SET = ROW_MASK;

    /*Initialize columns as return lines(inputs)*/
    LPC_IOCON->PIN_COL0 = 0x20;                      /* P3.21 is GPIO-KeyCol0*/
    LPC_IOCON->PIN_COL1 = 0x20;                      /* P3.22 is GPIO-KeyCol1*/
    LPC_IOCON->PIN_COL2 = 0x20;                      /* P3.23 is GPIO-KeyCol2*/
    LPC_IOCON->PIN_COL3 = 0x20;                      /* P3.24 is GPIO-KeyCol3*/

    /* set the pins as inputs and set the mask register*/
    COL_PORT->DIR &= ~COL_MASK;

    /*Initialize  keypad interrupt if defiend*/
#ifdef KEYPAD_INT_BASED
    ROW_PORT->CLR = ROW_MASK;
    LPC_IOCON->KEYPAD_INT_PORT |= 0x80;              /* P0.26 is GPIO-Interrupt*/

    LPC_GPIOINT->IO0IntEnR |= (1UL<<KEYPAD_INT_PIN); /* P0.26 - rising edge interrupt enabled*/
    LPC_GPIOINT->IO0IntClr |= (1UL<<KEYPAD_INT_PIN);     /* P0.26 - clears interrupt */
    NVIC_ClearPendingIRQ (GPIO_IRQn);
// NVIC_EnableIRQ(GPIO_IRQn);
#endif
}






/*****************************************************************************
* @note       Function name: U6 keypad_scan (void )
* @returns    returns      : keypad status U16
* @param      arg1         : none 
* @author                  : Swapnil Phirke 
* @date       date created :  March 2018
* @brief      Description  : Scan a particular row and check which buttons
                             pressed by reading the return lines using polling 
* @note       Notes        : None
*****************************************************************************/
uint16_t keypad_scan (void)
{
    uint8_t u8_status = NO_KEY_PRESSED_CODE;
		uint16_t u16_key_status = NO_KEY_PRESSED_CODE;
    uint32_t u32_pinVal = 0;
		uint8_t u8_row_num;
		static uint8_t ar_u8_Debounce_Buff[MAX_NO_KEYS]; 
		uint8_t u8_key ;
//	unsigned int j;
    /* reset the scan lines to output 1 */
    
	
	
	for(u8_row_num = 0;u8_row_num <4;u8_row_num++)
	{
    ROW_PORT->SET = ROW_MASK;
    switch (u8_row_num)
    {
					case KEY_ROW0:
							/*Check if any button in row0 is pressed*/
							ROW_PORT->CLR = (1UL<<IO_ROW0);
							u8_status = (1<<4);
							break;

					case KEY_ROW1:
							/*Check if any button in row1 is pressed*/
							ROW_PORT->CLR = (1UL<<IO_ROW1);
							u8_status = (1<<5);
							break;

					case KEY_ROW2:
							/*Check if any button in row2 is pressed*/
							ROW_PORT->CLR = (1UL<<IO_ROW2);
							u8_status = (1<<6);
							break;

					case KEY_ROW3:
							/*Check if any button in row3 is pressed*/
							ROW_PORT->CLR = (1UL<<IO_ROW3);
							u8_status = (1<<7);
							break;

					default:
							break;
					}
			//    os_dly_wait(1);
					osDelay(1);

					u32_pinVal = (COL_PORT->PIN);          /*Read the input register for column values*/
					u32_pinVal = u32_pinVal;

					if (((u32_pinVal & (1UL<<IO_COL0)) == 0) ||
									((u32_pinVal & (1UL<<IO_COL1)) == 0) ||
									((u32_pinVal & (1UL<<IO_COL2)) == 0) ||
									((u32_pinVal & (1UL<<IO_COL3)) == 0))
					{
							/*Read the input register AGAIN for column values*/
							if ((u32_pinVal & (1UL<<IO_COL0)) == 0) /*Check if any button in col0 is pressed*/
							{
									u8_status |= (1<<0);
							}
							if ((u32_pinVal & (1UL<<IO_COL1)) == 0) /*Check if any button in col1 is pressed*/
							{
									u8_status |= (1<<1);
							}
							if ((u32_pinVal & (1UL<<IO_COL2)) == 0) /*Check if any button in col2 is pressed*/
							{
									u8_status |= (1<<2);
							}
							if ((u32_pinVal & (1UL<<IO_COL3)) == 0) /*Check if any button in col3 is pressed*/
							{
									u8_status |= (1<<3);
							}
							
							switch (u8_row_num)
								{
										case KEY_ROW0:
											if((u32_pinVal & (1UL<<IO_COL0)) == 0)
											{
												//SET_BIT(key_status , KEY0);
												ar_u8_Debounce_Buff[KEY0] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY0],SET_KEY);
														if(ar_u8_Debounce_Buff[KEY0] == FULL_SET_DEBOUNCE)
														{
															SET_BIT(u16_key_status , KEY0);
															SET_BIT(Keypad_Direction , KEY0);													
														}
											}
											if((u32_pinVal & (1UL<<IO_COL1)) == 0)
											{
												//SET_BIT(key_status , KEY1);
												ar_u8_Debounce_Buff[KEY1] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY1],SET_KEY);
														if(ar_u8_Debounce_Buff[KEY1] == FULL_SET_DEBOUNCE)
														{
															SET_BIT(u16_key_status , KEY1);
															SET_BIT(Keypad_Direction , KEY1);
														}
											}
											if((u32_pinVal & (1UL<<IO_COL2)) == 0)
											{
												//SET_BIT(key_status , KEY2);
												ar_u8_Debounce_Buff[KEY2] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY2],SET_KEY);
														if(ar_u8_Debounce_Buff[KEY2] == FULL_SET_DEBOUNCE)
														{
															SET_BIT(u16_key_status , KEY2);
															SET_BIT(Keypad_Direction , KEY2);
														}
											}
											if((u32_pinVal & (1UL<<IO_COL3)) == 0)
											{
												//SET_BIT(key_status , KEY3);
												ar_u8_Debounce_Buff[KEY3] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY3],SET_KEY);
														if(ar_u8_Debounce_Buff[KEY3] == FULL_SET_DEBOUNCE)
														{
															SET_BIT(u16_key_status , KEY3);
															SET_BIT(Keypad_Direction , KEY3);
														}
											}
													
											break;

										case KEY_ROW1:
											if ((u32_pinVal & (1UL<<IO_COL0)) == 0)
											{
												//SET_BIT(key_status , KEY4);
												ar_u8_Debounce_Buff[KEY4] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY4],SET_KEY);
														if(ar_u8_Debounce_Buff[KEY4] == FULL_SET_DEBOUNCE)
														{
															SET_BIT(u16_key_status , KEY4);
															SET_BIT(Keypad_Direction , KEY4);
														}
											}
											if ((u32_pinVal & (1UL<<IO_COL1)) == 0)
											{
												//SET_BIT(key_status , KEY5);
												ar_u8_Debounce_Buff[KEY5] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY5],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY5] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY5);
																SET_BIT(Keypad_Direction , KEY5);
															}
											}
											if((u32_pinVal & (1UL<<IO_COL2)) == 0)
											{
												//SET_BIT(key_status , KEY6);
												ar_u8_Debounce_Buff[KEY6] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY6],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY6] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY6);
																SET_BIT(Keypad_Direction , KEY6);
															}
											}
											if ((u32_pinVal & (1UL<<IO_COL3)) == 0)
											{
												//SET_BIT(key_status , KEY7);
												ar_u8_Debounce_Buff[KEY7] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY7],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY7] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY7);
																SET_BIT(Keypad_Direction , KEY7);
															}
											}												

											break;
										case KEY_ROW2:
											if ((u32_pinVal & (1UL<<IO_COL0)) == 0)
											{
												//SET_BIT(key_status , KEY8);
												ar_u8_Debounce_Buff[KEY8] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY8],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY8] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY8);
																SET_BIT(Keypad_Direction , KEY8);
															}
											}
											if((u32_pinVal & (1UL<<IO_COL1)) == 0)
											{
												//SET_BIT(key_status , KEY9);
												ar_u8_Debounce_Buff[KEY9] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY9],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY9] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY9);
																SET_BIT(Keypad_Direction , KEY9);
															}
											}
											if((u32_pinVal & (1UL<<IO_COL2)) == 0)
											{
												//SET_BIT(key_status , KEY10);
												ar_u8_Debounce_Buff[KEY10] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY10],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY10] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY10);
																SET_BIT(Keypad_Direction , KEY10);
															}
											}
											if((u32_pinVal & (1UL<<IO_COL3)) == 0)
											{
												//SET_BIT(key_status , KEY11);
												ar_u8_Debounce_Buff[KEY11] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY11],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY11] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY11);
																SET_BIT(Keypad_Direction , KEY11);
															}
											}												

											break;
										case KEY_ROW3:
											if((u32_pinVal & (1UL<<IO_COL0)) == 0)
											{
												//SET_BIT(key_status , KEY12);
												ar_u8_Debounce_Buff[KEY12] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY12],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY12] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY12);
																SET_BIT(Keypad_Direction , KEY12);
															}
											}
											if ((u32_pinVal & (1UL<<IO_COL1)) == 0)
											{
												//SET_BIT(key_status , KEY13);
												ar_u8_Debounce_Buff[KEY13] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY13],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY13] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY13);
																SET_BIT(Keypad_Direction , KEY13);
															}
											}
											if ((u32_pinVal & (1UL<<IO_COL2)) == 0)
											{
												//SET_BIT(key_status , KEY14);
												ar_u8_Debounce_Buff[KEY14] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY14],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY14] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY14);
																SET_BIT(Keypad_Direction , KEY14);
															}
											}
											if ((u32_pinVal & (1UL<<IO_COL3)) == 0)
											{
												//SET_BIT(key_status , KEY15);
												ar_u8_Debounce_Buff[KEY15] = Set_Debounce_Buff(ar_u8_Debounce_Buff[KEY15],SET_KEY);
															if(ar_u8_Debounce_Buff[KEY15] == FULL_SET_DEBOUNCE)
															{
																SET_BIT(u16_key_status , KEY15);
																SET_BIT(Keypad_Direction , KEY15);
															}
											}												
										break;
										default:
												break;
								}
					}
					else
					{
//							key_status = NO_KEY_PRESSED_CODE;
					
//							for(key = 0;key<MAX_NO_KEYS;key++)
//							{
//								if(Debounce_Buff[key] != FULL_RESET_DEBOUNCE)
//								{
//									Debounce_Buff[key] = Set_Debounce_Buff(Debounce_Buff[key],RESET_KEY);
//												if(Debounce_Buff[key] == FULL_RESET_DEBOUNCE)
//												{
//													RESET_BIT(key_status , key+1 ); 				//Add 1 because micro starts with 1 and array starts from 0
//													SET_BIT(Keypad_Direction , key);
//												}
//								}
//							}
						
							for(u8_key = (u8_row_num * 4);u8_key<((u8_row_num +1) * 4) ; u8_key++)
							{
								if(ar_u8_Debounce_Buff[u8_key] != FULL_RESET_DEBOUNCE)
								{
									ar_u8_Debounce_Buff[u8_key] = Set_Debounce_Buff(ar_u8_Debounce_Buff[u8_key],RESET_KEY);
												if(ar_u8_Debounce_Buff[u8_key] == FULL_RESET_DEBOUNCE)
												{
													RESET_BIT(u16_key_status , u8_key+1 ); 				//Add 1 because micro starts with 1 and array starts from 0
													SET_BIT(Keypad_Direction , u8_key);
												}
								}
							}
						
					}
		
	  }

    return(u16_key_status);
}

uint8_t Set_Debounce_Buff(uint8_t Debounce_Buffer , uint8_t stuff_bit)
{
	
			Debounce_Buffer=Debounce_Buffer << 1;
			
			if(stuff_bit == SET_KEY)
			{
				SET_BIT(Debounce_Buffer , 0);
			}else	
			if (stuff_bit == RESET_KEY)
			{
//				RESET_BIT(Debounce_Buffer, 0);
			}			
				
	return 	Debounce_Buffer;	
}

