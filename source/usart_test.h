


#ifndef _UART_TEST_H_
# define _UART_TEST_H_	/* prevent multiple includes */

// include parameter **************************************************************************************

#include "Driver_USART.h"
#include "Keypad.h"
#include "Thread_param.h"


//define parameter*****************************************************************************************

//#define COMMOND_END_CHARACTER   13    //we are .
#define COMMOND_END_CHARACTER   '\n'
#define MAX_UART_IN_Q_SIZE   		32      // size of array queue in .
#define BAUD_RATE   							230400

#define POLLING_UART_SEND_Q 		1			//polling time for console task
#define MAX_UART_PRINT_LENGTH   50

//extern**************************************************************************************

extern osThreadId_t tid_UART_receive ,tid_uart_send_thread;

extern osMessageQueueId_t Q_UART_receive ,Q_uart_send_thread_IN; 

extern	 ARM_DRIVER_USART Driver_USART1;


//strucute/union***************************************************************************************
	typedef struct {                                                   // object data type
		int data;

	} MSGQUEUE_OBJ_t;
	
	
		typedef struct {                                                   // object data type
			char 		direction[2];
			char  	cmd_hi;
			char    cmd_lo;
			char    ascii;
			char    payload[MAX_UART_IN_Q_SIZE];  																		//payload has to be in multiple of 4 
			
	} UART_SEND_IN_STRU;

	
	
//		typedef struct {                                                   // object data type
//		char data[MAX_UART_IN_Q_SIZE];

//	} UART_SEND_IN_STRU;
	
	
//*****function declearation************************************************************************
	
//	void print_Number(ARM_DRIVER_USART* USARTx, uint16_t x);
	 void USART_receive_callback(uint32_t event);

#endif /* _UART_TEST_H_ */
	

	

	