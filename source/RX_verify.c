#include "cmsis_os2.h"                                        // CMSIS RTOS header file
#include "RX_verify.h"
 
 
 /* .................................Meaasage Queue...................................*/
		extern osMessageQueueId_t Q_UART_receive;                                   // message queue id
				
		extern osMessageQueueId_t Q_Consol_Task_IN;
 
/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 

osThreadId_t tid_RX_Verify;                                      // thread id
extern osThreadId_t tid_Consol_Task;

 
int Init_RX_Verify (void) {
		osThreadAttr_t attr =
		{
			.name = "RX_Verify",
			.attr_bits = osThreadDetached,
			.cb_mem = NULL,
			.cb_size = 0,
			.stack_mem = NULL,
			.stack_size = 1024,
//			.priority = osPriorityNormal,
			.priority = RX_VERIFY_PRIORITY,
		};
	
		tid_RX_Verify = osThreadNew (RX_Verify, NULL, &attr);
  
  return tid_RX_Verify != NULL;
}
 
//void RX_Verify (void *argument);
void RX_Verify (void *argument) {
	
//	osThreadId_t id; 
//	osStatus_t   status;                                      // status of the executed function
	char RX_data[MAX_COMMOND_LINE_BUFFER];
	static int length_of_string;
	char buffer_data[MAX_PAYLOAD_SIZE_CONSOLE_IN];
	
	MSGQUEUE_OBJ_t msg;
  osStatus_t status_queue;
	
	CONSOL_TASK_STRU_1 s_uart_console_in;
	WRAPPER_INPUT s_wrapper_input_uart;
 
	s_wrapper_input_uart.cmd_hi  = COMMOND_CON_UART_HI;
	s_wrapper_input_uart.cmd_lo  = COMMOND_CON_UART_LOW;
	
	//s_wrapper_input_uart.payload_size = sizeof((                payload size pending 
	
	s_wrapper_input_uart.response_type = INPUT_RESPONSE;
	s_wrapper_input_uart.payload_type  = PAYLOAD_NONE;
	
  while (1) {
    ; // Insert thread code here...
		
		osThreadFlagsWait(FLAG_UART_RX_VERIFY, NULL, osWaitForever);						//Wait to receive data 
		status_queue = osMessageQueueGet (Q_UART_receive, &msg, NULL, NULL);  // Check if some data in queue 
		
		length_of_string = 0;
    while (status_queue == osOK) 
			{					
						RX_data[length_of_string] = msg.data;
//						osMessageQueuePut (Q_Consol_Task_IN, &msg, 0, NULL);
  					status_queue = osMessageQueueGet (Q_UART_receive, &msg, NULL, NULL);
						length_of_string++;
//						osMessageQueuePut (Q_Consol_Task_IN, &msg, 0, NULL);
			}
			
			s_wrapper_input_uart.payload_size	=	length_of_string ; 
			
			strcpy(buffer_data,RX_data);
			strcat(buffer_data ,END_CHAR_DATA);
			
			osMutexAcquire(mutex_is_WrapConsole_data, osWaitForever);
			WrapConsole_data(buffer_data,s_wrapper_input_uart , &s_uart_console_in);
			osMutexRelease(mutex_is_WrapConsole_data);
			osMessageQueuePut (Q_Consol_Task_IN ,&s_uart_console_in ,0,NULL);

  }
}


