#include "usart_test.h"
#include "cmsis_os2.h"                   /* ARM::CMSIS:RTOS:Keil RTX */
#include <stdio.h>
#include <string.h>


/* .................................Theread ID...................................*/
extern osThreadId_t tid_RX_Verify;


osThreadId_t tid_UART_receive ,tid_uart_send_thread;
//extern ARM_DRIVER_USART Driver_USART1;

/* .................................Queue ID...................................*/
osMessageQueueId_t Q_UART_receive ,Q_uart_send_thread_IN; 

 
 static uint64_t uart_stack[2048/8] __attribute__((section("USB_RAM_TASK"))); 
// static void * uart_queue_stack  ; //__attribute__((section("UART_RAM_QUEUE")));
/* USART Driver */
// void myUSART_callback(uint32_t event)
 void USART_receive_callback(uint32_t event)
{
  uint32_t mask;
	uint32_t status_flag;

  mask = ARM_USART_EVENT_RECEIVE_COMPLETE  |
         ARM_USART_EVENT_TRANSFER_COMPLETE |
         ARM_USART_EVENT_SEND_COMPLETE     |
         ARM_USART_EVENT_TX_COMPLETE       ;

  if (event & mask) {
    /* Success: Wakeup Thread */
    status_flag = osThreadFlagsSet(tid_UART_receive, FLAG_UART_RECEIVE);
  }

  if (event & ARM_USART_EVENT_RX_TIMEOUT) {
    __breakpoint(0);  /* Error: Call debugger or replace with custom error handling */
  }

  if (event & (ARM_USART_EVENT_RX_OVERFLOW | ARM_USART_EVENT_TX_UNDERFLOW)) {
    __breakpoint(0);  /* Error: Call debugger or replace with custom error handling */
  }
}

/* CMSIS-RTOS Thread - UART command thread */
void UART_receive(void* arg)
{

		ARM_DRIVER_USART * const USARTdrv = arg;
    static char   cmd;
		MSGQUEUE_OBJ_t msg;
		osStatus_t S_Q_UART_receive;
 
  #ifdef DEBUG
    ARM_DRIVER_VERSION     version;
    ARM_USART_CAPABILITIES drv_capabilities;

		version = USARTdrv->GetVersion();
    if (version.api < 0x200)   /* requires at minimum API version 2.00 or higher */
    {                          /* error handling */
        return;
    }
    drv_capabilities = USARTdrv->GetCapabilities();
    if (drv_capabilities.event_tx_complete == 0)
    {                          /* error handling */
        return;
    }
  #endif
 
    /*Initialize the USART driver */
    USARTdrv->Initialize(USART_receive_callback);
    /*Power up the USART peripheral */
    USARTdrv->PowerControl(ARM_POWER_FULL);
    /*Configure the USART to 4800 Bits/sec */
    USARTdrv->Control(ARM_USART_MODE_ASYNCHRONOUS |
                      ARM_USART_DATA_BITS_8 |
                      ARM_USART_PARITY_NONE |
                      ARM_USART_STOP_BITS_1 |
                      ARM_USART_FLOW_CONTROL_NONE, BAUD_RATE);
//     USARTdrv->Control(A
    /* Enable Receiver and Transmitter lines */
    USARTdrv->Control (ARM_USART_CONTROL_TX, 1);
    USARTdrv->Control (ARM_USART_CONTROL_RX, 1);
 
 
 	
	const osMessageQueueAttr_t attr =
	{
			.name = "USB_queue",
			.attr_bits = 0,
			.cb_mem = NULL,
			.cb_size = 0,
			.mq_mem = &uart_stack,
			.mq_size = (2048/8),
//			.priority = osPriorityNormal,
//			.attr_bits = 0,
	};

	  Q_UART_receive = osMessageQueueNew(MSGQUEUE_OBJECTS, sizeof(MSGQUEUE_OBJ_t), &attr);
		if (!Q_UART_receive) {
			__NOP();					; // Message Queue object not created, handle failure
			}
	
		
    while (1)
    {
			
//			osDelay(100);
			USARTdrv->Receive(&cmd, 1);          /* Get byte from UART */
			osThreadFlagsWait(FLAG_UART_RECEIVE, NULL, 10);  
//			osThreadFlagsWait(FLAG_UART_RECEIVE, NULL, osWaitForever);  
			
//    
							if(cmd != NULL)
							{
			if (cmd == COMMOND_END_CHARACTER)                       /* CR, send greeting  */
        {

								msg.data = cmd;
								S_Q_UART_receive = osMessageQueuePut(Q_UART_receive, &msg, 0, NULL);
								if(S_Q_UART_receive == osErrorResource) // If queue is full we are removing the initial data from queue it is lossy queue 
								{
									osMessageQueueGet(Q_UART_receive,NULL,NULL,NULL); //Removing packet from queue 
									osMessageQueuePut(Q_UART_receive, &msg, 0, NULL); //adding data in queue 
								}
								cmd = NULL;
								osThreadFlagsClear(FLAG_UART_RECEIVE);
					
								osThreadFlagsSet(tid_RX_Verify, FLAG_UART_RX_VERIFY);
//								osThreadFlagsWait(FLAG_UART_RECEIVE, NULL, osWaitForever);
        }
		 msg.data = cmd;
		 S_Q_UART_receive = osMessageQueuePut (Q_UART_receive, &msg, 0, NULL);
		 	osThreadFlagsClear(FLAG_UART_RECEIVE);	
		 
			if(S_Q_UART_receive == osErrorResource)              // If queue is full we are removing the initial data from queue it is lossy queue 
			{
				osMessageQueueGet(Q_UART_receive,NULL,NULL,NULL);  //Removing packet from queue 
				osMessageQueuePut(Q_UART_receive, &msg, 0, NULL);  //adding data in queue 
			}				
//			osThreadFlagsSet(tid_RX_Verify, FLAG_UART_RX_VERIFY); 	
		}
	}
}

/* create the UART test thread */
int init_uart_receive_thread(	ARM_DRIVER_USART *pUsart)
	{
	osThreadAttr_t attr =
		{
			.name = "UART_receive",
			.attr_bits = osThreadDetached,
			.cb_mem = NULL,
			.cb_size = 0,
			.stack_mem = NULL,
			.stack_size = 256,
			.priority = MY_UART_PRIORITY,
		};

	tid_UART_receive = osThreadNew(
												UART_receive,
												/* argument */ (void *) pUsart,
												/* attributes */ &attr
												);                                // create thread and save value to tid
		
	return tid_UART_receive != NULL;								
	}
	
	
	
	
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	////////////////////////////////////UART_SEND////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/***********************************************************************************
* Funcrion   :- init_uart_send_thread(ARM_DRIVER_USART)
* Input param:- ARM_DRIVER_USART  
* Output param :- bool
* Description :- It will create thread to send data on UART	
*
**************************************************************************************/

	/* create the UART test thread */
int init_uart_send_thread(	ARM_DRIVER_USART *pUsart)
	{
	osThreadAttr_t attr =
		{
			.name = "UART_send",
			.attr_bits = osThreadDetached,
			.cb_mem = NULL,
			.cb_size = 0,
			.stack_mem = NULL,
			.stack_size = 256,
			.priority = MY_UART_PRIORITY,
		};																										// create thread and save value 

	tid_uart_send_thread = osThreadNew(
												uart_send_thread,
												/* argument */ (void *) pUsart,
												/* attributes */ &attr
												);
		
	return tid_uart_send_thread != NULL;								
	}
/*******************************************************************************
*Function   :- UART data send 
*Input      :- Argument pass through thread creation  
*Description :- it will receive data from console task and it will print on UART
*
*	
********************************************************************************/	
	
	
void uart_send_thread(void* arg)
{
    ARM_DRIVER_USART * const USARTdrv = arg;
//	   ARM_DRIVER_USART * const USARTdrv  = &Driver_USART1;// = arg;

		osStatus_t status_queue;
		uint32_t string_length;
		UART_SEND_IN_STRU q_receive_data;
		static const char InitMsg[] = "\r\n debug task created\r\n";
		static char uart_print[MAX_UART_PRINT_LENGTH];	
		Q_uart_send_thread_IN = osMessageQueueNew(UART_IN_QUEUE_SIZE, sizeof(UART_SEND_IN_STRU), NULL);     // create queue for data in

		USARTdrv->Send(InitMsg, sizeof(InitMsg));
	
		while(1)
		{

						status_queue = osMessageQueueGet(Q_uart_send_thread_IN , &q_receive_data, NULL, osWaitForever); //receive data through queue 
				    if(status_queue == osOK)   // repeat iteration till there is data in queue 
						{					
							memset(uart_print,NULL,MAX_PAYLOAD_SIZE_CONSOLE_IN); // clear memory 
//							memcpy(uart_print,&q_receive_data,MAX_UART_IN_Q_SIZE);
							
							memcpy(uart_print,&q_receive_data,sizeof(UART_SEND_IN_STRU));

							string_length = strlen(uart_print);
								
							USARTdrv->Send(uart_print, strlen(uart_print));	          //send data through uart 
					
						}else{
							//handle error 
						}		
		}//end of loop

}//end for thread 

