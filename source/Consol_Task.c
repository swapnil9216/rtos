#include "cmsis_os2.h"                                        // CMSIS RTOS header file
//#include "Keypad.h"
#include "Consol_Task.h" 
 
 /* .................................Queue ID...................................*/
osMessageQueueId_t Q_Consol_Task_IN;
extern osMessageQueueId_t  Q_Keypad_task_OUT ;
 

 
/*----------------------------------------------------------------------------
 *      Thread 1 'Thread_Name': Sample thread
 *---------------------------------------------------------------------------*/
 
                                // thread function
osThreadId_t tid_Consol_Task;                                      // thread id
 
int Init_Consol_Task (void) {
 
		osThreadAttr_t attr =
		{
			.name = "Consol_Task",
			.attr_bits = osThreadDetached,
			.cb_mem = NULL,
			.cb_size = 0,
			.stack_mem = NULL,
			.stack_size = 1024,
//			.priority = osPriorityNormal,
			.priority = CONSOL_TASK_PRIORITY,
		};
		
			
		tid_Consol_Task = osThreadNew (Consol_Task, NULL, &attr);
  
  return tid_Consol_Task != NULL;
}
 
void Consol_Task (void *argument) {

	CONSOL_TASK_STRU_1 msg;
	osStatus_t status_queue;

	Q_Consol_Task_IN  =  osMessageQueueNew(CONSOL_QUEUE_SIZE, sizeof(CONSOL_TASK_STRU_1), NULL);  //In queue initilize
  while (1) {
							status_queue = osMessageQueueGet (Q_Consol_Task_IN , &msg, NULL, osWaitForever);  // check if there is some data in queue 

							if (status_queue == osOK) 
							{					
									commond_process(&msg);  																								//process input commond 			
								
							}else{
								//Handle data error 
							}
						}
}

/**************************************************************************
* Function   :- commond_process
* Input      :- console task input structure pointer 
* Description :- It will process the commond id 
*
****************************************************************************/


void commond_process(CONSOL_TASK_STRU_1 *msg)
{
	uint32_t commond_hi;												
//	UART_SEND_IN_STRU q_send_uart_data;          // need to define this function using malloc in future
	WRAPPER_INPUT wrapper_data_console;
	CONSOL_TASK_STRU_1 q_send_uart_data ;
	char buffer_data[MAX_PAYLOAD_DATA];
	USB_IN_STRUCT  s_USB_queue_data;
	static uint8_t count;
	
	//USB create temperary veriable will remove in actual built
	uint8_t usb_commond_id;
	char folder_name[FILE_NAME_SIZE];
  char file_name[FOLDER_NAME_SIZE];	
	char payload_usb[USB_QUEUE_PAYLOAD];
	
	
	
	
	commond_hi = msg->Cmd_hi ;									// check which commond id we are using 
	commond_hi =  COMMOND_CON_USB ;         //  testing purpose 
	switch(commond_hi)													// process to the commond id 										
	{
		case COMMOND_CON_KEYPAD :											// commond id keypads 
		{			

			
//				q_send_uart_data.direction[0] = msg->direction[0];                // fist to show input commond 
//				q_send_uart_data.direction[1] = msg->direction[1];								// fist to show input commond 
			
				wrapper_data_console.response_type =	msg->direction[0];
				
				wrapper_data_console.payload_type = PAYLOAD_NONE;
				
				wrapper_data_console.cmd_hi = msg->Cmd_hi ;										// cmd_Hi
				wrapper_data_console.cmd_lo = msg->Cmd_lo ;										//// cmd_low
			
				wrapper_data_console.payload_size = strlen(msg->payload);    //find length of string 
				strcpy(buffer_data,msg->payload);															//copy payload dat in string 
				
				osMutexAcquire(mutex_is_WrapConsole_data, osWaitForever);
				WrapConsole_data(buffer_data,wrapper_data_console ,&q_send_uart_data);
				osMutexRelease(mutex_is_WrapConsole_data);
								
					osMessageQueuePut (Q_uart_send_thread_IN,&q_send_uart_data, 0, NULL);   // put commond in queue  for uart send 
		}
		break;		
		
		case COMMOND_CON_IDEAL  :
		{

				wrapper_data_console.response_type =	msg->direction[0];
			
				wrapper_data_console.payload_type = PAYLOAD_NONE;							//we need to copy input data directly on UART 
				
				wrapper_data_console.cmd_hi = msg->Cmd_hi ;										// cmd_Hi
				wrapper_data_console.cmd_lo = msg->Cmd_lo ;										//// cmd_low
			
				wrapper_data_console.payload_size = strlen(msg->payload);			// find length of payload
				strcpy(buffer_data,msg->payload);															//copy payload in buffer for queue 
				
				osMutexAcquire(mutex_is_WrapConsole_data, osWaitForever);               //Mutex is used so it should be able to use by only one task
				WrapConsole_data(buffer_data,wrapper_data_console ,&q_send_uart_data);
				osMutexRelease(mutex_is_WrapConsole_data);

				osMessageQueuePut (Q_uart_send_thread_IN, &q_send_uart_data, 0, NULL);   // put commond in queue  for uart send 
			break;
		}
				case COMMOND_CON_USB :
		{
				sprintf(folder_name,"print data ");
				sprintf(file_name,"data.txt");
				sprintf(payload_usb,"%d payload append \r\n",count++);
//				s_USB_queue_data.usb_commond_id = USB_BACKUP_COMMOND;
				
				wrapper_data_console.response_type = OUTPUT_RESPONSE;
			
					wrapper_data_console.payload_type = PAYLOAD_NONE;
				
				wrapper_data_console.cmd_hi  =  COMMOND_CON_USB;
				wrapper_data_console.cmd_lo  =  COMMOND_CON_USB_LOW;
			
				sprintf(buffer_data,"%c,%s,%s,%s",USB_APPEND_DATA_COMMOND,folder_name,file_name,payload_usb);
//				strcat(buffer_data ,END_CHAR_DATA);
			
				wrapper_data_console.payload_size = strlen(buffer_data);

			
				osMutexAcquire(mutex_is_WrapConsole_data, osWaitForever);               //Mutex is used so it should be able to use by only one task
				WrapConsole_data(buffer_data,wrapper_data_console ,&q_send_uart_data);
				osMutexRelease(mutex_is_WrapConsole_data);
			
				
			
				osMessageQueuePut (Q_USB_Task_IN, &q_send_uart_data, 0, NULL); 

		break;	
		}
	}	
}
	