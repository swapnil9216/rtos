/*----------------------------------------------------------------------------
 * CMSIS-RTOS 'main' function template
 *---------------------------------------------------------------------------*/
#include "main.h"
#include "Consol_Task.h"
#include "Keypad.h"
//Variable Declaration
//#include "rl_fs.h"
//#include "rl_usb.h"
		uint32_t app1,app2,app3;

/* .................................Theread ID...................................*/
osThreadId_t g_ID_Thread1 , g_ID_Thread2 ,g_ID_app_main,thread_UART;


/* .................................Meaasage Queue...................................*/
		extern osMessageQueueId_t Q_UART_receive;                                   // message queue id


//__attribute__((section("GUI_RAM")));

/*----------------------------------------------------------------------------
 * Application main thread
 *---------------------------------------------------------------------------*/
void app_main (void *argument) {

  // ...
  for (;;) {
		osDelay(100);
	app1++;
	}
}

void Thread1 (void *argument) {
	uint32_t i,j = 0;
	osThreadId_t id; 
	osStatus_t   status;                                      // status of the executed function
   
  
  id = osThreadGetId ();                                    // Obtain ID of current running thread
   
  status = osThreadSetPriority (id, osPriorityNormal1); // Set thread priority
  if (status == osOK)  {
    // Thread priority changed to BelowNormal
  }
  else {
    // Failed to set the priority
  }
	
	
  for (;;) {
		for(i=0;i<1000;i++)for(j=0;j<1000;j++);
		osDelay(10);
	app2++;
	}
}
 uint32_t old_systick,current_systick,diff_systick;


void Thread2 (void *argument) {
	osThreadId_t id; 
	osStatus_t   status;                                      // status of the executed function
	char data_main;
	
	MSGQUEUE_OBJ_t msg;
  osStatus_t status_queue;

  
  id = osThreadGetId ();                                    // Obtain ID of current running thread
   
  status = osThreadSetPriority (id, osPriorityNormal); // Set thread priority
  if (status == osOK)  {
    // Thread priority changed to BelowNormal
  }
  else {
    // Failed to set the priority
  }
	
	
  // ...
  for (;;) {
		
		osDelay(1);

	app3++;
	}
}
 //function to detecct hard fault

void HardFault_Handler()
{
   	osThreadId_t temp_id; 			//osthread id 
		temp_id = osThreadGetId();  // temp_id for hard fault
		__NOP();
}



int main (void) {
 
	
  // System Initialization
  SystemCoreClockUpdate();
//	Initilize_UART();
#ifdef RTE_Compiler_EventRecorder
  // Initialize and start Event Recorder
  EventRecorderInitialize(EventRecordAll, 1U);
#endif
  // ... 
//	USBH_Initialize (0U);
	 printf("initialising kernel\r\n");
  osKernelInitialize();                 // Initialize CMSIS-RTOS	
	

	createtask();													//createtask all new task in this function 

  osKernelStart();                      // Start thread execution
  for (;;) {}
}


/***********************************************************************************
* Funcrion   :- creaatetask(void)
* Input param:- None
* Output param :- None
* Description :- This function will create atll thread 
*
**************************************************************************************/



void createtask()	
{
	
	
//	  mid_MsgQueue = osMessageQueueNew(MSGQUEUE_OBJECTS, sizeof(MSGQUEUE_OBJ_t), NULL);
//		if (!mid_MsgQueue) {
//			; // Message Queue object not created, handle failure
//			}
	
//		g_ID_app_main = osThreadNew(app_main, NULL, NULL);    // Create application main thread
//		if(g_ID_app_main != NULL){
//			//Thread created 
//		}
//		
//	  g_ID_Thread1 = osThreadNew(Thread1, NULL, NULL);    // Create application main thread
//		if(g_ID_Thread1 != NULL){
//			//Thread created 
//		}
//		
//	  g_ID_Thread2 = osThreadNew(Thread2, NULL, NULL);    // Create application main thread
//		if(g_ID_Thread2 != NULL){
//			//Thread created 
//		}
//		
		
		  if(!Init_Consol_Task())												//create thred for console task 
		 {
			  __NOP;//thread not created 
		 }
		 
			if(!Init_Keypad_Task())											//ini keypad task 
		 {
			  __NOP;//thread not created 
		 }
	
		 if (! init_uart_receive_thread(&Driver_USART1))     //create thread for uard receive
		 {
			 __NOP;//thread not created 
		 }
		 
		 if (! init_uart_send_thread(&Driver_USART1))      //create thread for uart send
		 {
			 __NOP;//thread not created 
		 }
		 if(!Init_RX_Verify())													//create thread to verify RX thread create 
		 {
			  __NOP;//thread not created 
		 }
		 if(!Init_USB_Task())													//create thread for USB_task
		 {
			  __NOP;//thread not created 
		 }
//		 
//		 

}