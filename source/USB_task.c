#include "cmsis_os2.h"                                        // CMSIS RTOS header file
//#include "Keypad.h"
#include "USB_task.h" 





                              
osThreadId_t tid_USB_Task;                                      // USB thread id
osMessageQueueId_t Q_USB_Task_IN;                               // USB_Que_id

//static uint64_t usb_queue_stack[4098/8] __attribute__((section("USB_RAM_QUEUE")));
static void * usb_queue_stack   __attribute__((section("USB_RAM_QUEUE")));
 
static uint64_t usb_stack[2048/8] __attribute__((section("USB_RAM_TASK"))); 
// static uint64_t *usb_stack __attribute__((section("USB_RAM"))); 
int Init_USB_Task (void) {
 
		osThreadAttr_t attr =
		{
			.name = "USB_Task",
			.attr_bits = osThreadDetached,
			.cb_mem = NULL,
			.cb_size = 0,
			.stack_mem = usb_stack,
			.stack_size =(uint32_t)sizeof(usb_stack),
//			.priority = osPriorityNormal,
			.priority = USB_TASK_PRIORITY,
		};
		
			
		tid_USB_Task = osThreadNew (USB_Task, NULL, &attr);
  
  return tid_USB_Task != NULL;
}
 



void USB_Task(void *argument) {

	usbStatus usb_status ;                 // USB status
  int32_t   msc_status;                 // MSC status

	
	USB_IN_STRUCT s_data_in;
	uint8_t usb_commond_id;
  osStatus_t status_queue;
	
	CONSOL_TASK_STRU_1 s_usb_data;
	USB_RETURN_STRUCT s_usb_return_stru;
	WRAPPER_INPUT s_wrapper_input_usb;
	char buffer_data[MAX_PAYLOAD_SIZE_CONSOLE_IN];
	
	
	s_wrapper_input_usb.cmd_hi  = COMMOND_CON_USB_RETUTN_HI;
	s_wrapper_input_usb.cmd_lo  = COMMOND_CON_USB_LOW;
	
	s_wrapper_input_usb.payload_size = sizeof(USB_RETURN_STRUCT);
	s_wrapper_input_usb.payload_type = PAYLOAD_NONE;
	
	s_wrapper_input_usb.response_type = INPUT_RESPONSE;

	
	const osMessageQueueAttr_t attr =
	{
			.name = "USB_queue",
			.attr_bits = 0,
			.cb_mem = NULL,
			.cb_size = 0,
			.mq_mem = usb_queue_stack,
			.mq_size = NULL,
//			.priority = osPriorityNormal,
//			.attr_bits = 0,
	};

	Q_USB_Task_IN  =  osMessageQueueNew(USB_QUEUE_SIZE, sizeof(CONSOL_TASK_STRU_1), &attr);  //In queue initilize
 
	usb_status =  USBH_Initialize (0U); 
	if (usb_status != usbOK)
	{
			for (;;) {}                         // Handle USB Host 0 init failure
	}
  while (1) 
		{
							status_queue = osMessageQueueGet (Q_USB_Task_IN , &s_usb_data, NULL, osWaitForever);  // check if there is some data in queue 
							if (status_queue == osOK) 
							{					
									commond_process_usb(&s_usb_data,&s_usb_return_stru);  																								//process input commond 			
									memset(&s_usb_data,NULL,sizeof(s_usb_data));
								  sprintf(buffer_data,"%c,%c,%s",s_usb_return_stru.operation_status,s_usb_return_stru.usb_commond_id,s_usb_return_stru.payload_usb_return);
									strcat(buffer_data ,END_CHAR_DATA);
								
									osMutexAcquire(mutex_is_WrapConsole_data, osWaitForever);
									WrapConsole_data(buffer_data,s_wrapper_input_usb , &s_usb_data);
									osMutexRelease(mutex_is_WrapConsole_data);
									osMessageQueuePut (Q_Consol_Task_IN ,&s_usb_data ,0,NULL);
								
							}else{
								//Handle data error 
							}

			}// End for while 
		

}



void commond_process_usb (CONSOL_TASK_STRU_1* s_data_consol ,USB_RETURN_STRUCT* s_usb_return)
{
			char commond_id;
			int32_t   msc_status; 
		  FILE     *F_file ,*backup_file; 
			USB_IN_STRUCT s_data_in ;
			uint8_t  status_file_operations,file_operation_responce;
			char   file_name_buff[FILE_NAME_SIZE];
			char   data_usb_payload[USB_QUEUE_PAYLOAD];
			char  buffer_char;
			uint32_t position;
			char buffer[MAX_PAYLOAD_DATA];
			char return_buffer[MAX_USB_RETURN_BUFFER];

			char *token;
			
	
	
//	this is logic to convert till comma 		
			memset(buffer,NULL,MAX_PAYLOAD_DATA);
			memcpy(buffer,s_data_consol->payload,MAX_PAYLOAD_DATA);
	
				memset(&s_data_in,NULL,sizeof(s_data_in));
			   token = strtok(buffer, ",");
        strcpy(s_data_in.usb_commond_id,token) ;   
				token = strtok(NULL, ",");
				strcpy(s_data_in.folder_name,token) ; 
				token = strtok(NULL, ",");
				strcpy(s_data_in.file_name,token) ; 
				token = strtok(NULL, NULL);
				strcpy(s_data_in.payload_usb,token) ;
	    
			
	
				memset(s_usb_return->payload_usb_return,NULL,sizeof(s_usb_return->payload_usb_return));
//			commond_id = s_data_in.usb_commond_id[0];
	 s_usb_return->usb_commond_id = s_data_in.usb_commond_id[0];
	
	switch(s_usb_return->usb_commond_id)
	{
		case USB_MOUNT_COMMOND :		
				{
						msc_status = USBH_MSC_DriveGetMediaStatus ("U0:");
							if (msc_status == USBH_MSC_OK) 
							{
								msc_status = USBH_MSC_DriveMount ("U0:");   
								if (msc_status == USBH_MSC_OK) 
								{
									s_usb_return->operation_status = USB_OPERATION_OK ; //we need to design responce to send this data 				
									
								}else {
									s_usb_return->operation_status = USB_OPERATION_ERROR ;
									sprintf(return_buffer,"%x",msc_status);
									strcpy(s_usb_return->payload_usb_return,return_buffer);
								}
							}	
				}			
		break;	
				
		case USB_UNMOUNT_COMMOND :
					{
							msc_status = USBH_MSC_DriveGetMediaStatus ("U0:");
								if (msc_status == USBH_MSC_OK) 
								{
										msc_status = USBH_MSC_DriveUnmount ("U0:");   
										if (msc_status == USBH_MSC_OK) 
										{
											s_usb_return->operation_status = USB_OPERATION_OK ;//we need to design responce to send this data 
											
										}else{
											s_usb_return->operation_status = USB_OPERATION_ERROR ;
											sprintf(return_buffer,"%x",msc_status);
									    strcpy(s_usb_return->payload_usb_return,return_buffer);
										}
								}	
					}
		break;
		
				case USB_APPEND_DATA_COMMOND :
				{
						msc_status = USBH_MSC_DriveGetMediaStatus ("U0:");
							if (msc_status == USBH_MSC_OK) 
							{
								msc_status = USBH_MSC_DriveMount ("U0:");  
								if (msc_status == USBH_MSC_OK) 
								{
										sprintf(file_name_buff,"%s",s_data_in.file_name);
										F_file = fopen (file_name_buff, "a");
										
		//								sprintf(data_usb_payload,"%s \n",s_data_in->payload_usb);
										file_operation_responce = fprintf (F_file,"%s",s_data_in.payload_usb);
										if(file_operation_responce != NULL)
										{
											s_usb_return->operation_status = USB_OPERATION_OK ;
											sprintf(return_buffer,"%x",file_operation_responce);
									    strcpy(s_usb_return->payload_usb_return,return_buffer);
										
										}
										fclose  (F_file); 				
								}else {
									
									s_usb_return->operation_status = USB_OPERATION_ERROR ;
											sprintf(return_buffer,"%x",msc_status);
									    strcpy(s_usb_return->payload_usb_return,return_buffer);
								}
							}	else {
									
									s_usb_return->operation_status = USB_OPERATION_ERROR ;
									sprintf(return_buffer,"%x",msc_status);
									strcpy(s_usb_return->payload_usb_return,return_buffer);
								}
					
				}
		break;
				
				case USB_CREATE_FILE_COMMOND :
				{
						msc_status = USBH_MSC_DriveGetMediaStatus ("U0:");
							if (msc_status == USBH_MSC_OK) 
							{
								msc_status = USBH_MSC_DriveMount ("U0:");   
								if (msc_status == USBH_MSC_OK) 
								{
										sprintf(file_name_buff,"%s",s_data_in.file_name);
										F_file = fopen (file_name_buff, "r");
										fclose  (F_file); 		
										if(F_file != NULL)
										{
											s_usb_return->operation_status = USB_OPERATION_OK ;
											sprintf(return_buffer,"%s",file_name_buff);
									    strcpy(s_usb_return->payload_usb_return,return_buffer);
											
										}
								}else {
									s_usb_return->operation_status = USB_OPERATION_ERROR ;
											sprintf(return_buffer,"%x",msc_status);
									    strcpy(s_usb_return->payload_usb_return,return_buffer);
								}
							}else {
									
									s_usb_return->operation_status = USB_OPERATION_ERROR ;
								}	
					
				}
		break;
				case USB_FORMAT_COMMOND :
				{
						msc_status = USBH_MSC_DriveGetMediaStatus ("U0:");
							if (msc_status == USBH_MSC_OK) 
							{
								msc_status = USBH_MSC_DriveMount ("U0:");   
								if (msc_status == USBH_MSC_OK) 
								{
								     file_operation_responce = fformat ("U0:","/FAT32");	
										 if(file_operation_responce != NULL)
										{
											s_usb_return->operation_status = USB_OPERATION_OK ;
//											sprintf(return_buffer,"%s",file_name_buff);
//									    strcpy(s_usb_return->payload_usb_return,return_buffer);
										}
								}else{
									
									   s_usb_return->operation_status = USB_OPERATION_ERROR ;
										 sprintf(return_buffer,"%x",msc_status);
									   strcpy(s_usb_return->payload_usb_return,return_buffer);
								}
								//fclose  (F_file); 
							}	else {
									
									s_usb_return->operation_status = USB_OPERATION_ERROR ;
									sprintf(return_buffer,"%x",msc_status);
									strcpy(s_usb_return->payload_usb_return,return_buffer);
								}
					
				}
		break;
				case USB_BACKUP_COMMOND :
				{
						msc_status = USBH_MSC_DriveGetMediaStatus ("U0:");
							if (msc_status == USBH_MSC_OK) 
							{
								msc_status = USBH_MSC_DriveMount ("U0:");   
								if (msc_status == USBH_MSC_OK) 
								{
									
										F_file = fopen(s_data_in.file_name, "r");
								   	sprintf(file_name_buff,"b_%s",s_data_in.file_name);  // in future we will put time stap on the same for naming 
										backup_file = fopen (file_name_buff, "w");
										

										
									  if(	backup_file != NULL)
										{
													fseek(F_file, 0L, SEEK_END);
													position = ftell(F_file);
													fseek(F_file, 0L, SEEK_SET);
													while (position--)
													{
															buffer_char = fgetc(F_file);  // copying file character by character
															fputc(buffer_char, backup_file);
													}  
											//need to make verification before moving forward 
											s_usb_return->operation_status = USB_OPERATION_OK ;
											sprintf(return_buffer,"%s",file_name_buff);
									    strcpy(s_usb_return->payload_usb_return,return_buffer);													
										}
									  fclose (backup_file);		
										fclose  (F_file); 
								}else{	
									   s_usb_return->operation_status = USB_OPERATION_ERROR ;
										 sprintf(return_buffer,"%x",msc_status);
									    strcpy(s_usb_return->payload_usb_return,return_buffer);
								}					
								
							}	else {
									
									s_usb_return->operation_status = USB_OPERATION_ERROR ;
									sprintf(return_buffer,"%x",msc_status);
									strcpy(s_usb_return->payload_usb_return,return_buffer);								
								}
					
				}
		break;
		default :
				{
					s_usb_return->operation_status = USB_OPERATION_ERROR ;
					sprintf(return_buffer,"commond id error ");
					strcpy(s_usb_return->payload_usb_return,return_buffer);
				}
		break;
	}
		
}