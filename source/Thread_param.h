//This file contain all parameter regarding Thread 






#ifndef __THREAD_PARAM_H
#define __THREAD_PARAM_H

//#include "RTE_Components.h"
//#include  CMSIS_device_header
//#include "cmsis_os2.h"
// #include <lpc17xx.h>
//#include "usart_test.h"
//#include "usart_test.h"
//#include "Thread_param.h"
//#include "RX_verify.h"
 
//#ifdef RTE_Compiler_EventRecorder
//#include "EventRecorder.h"
//#endif


#include<stdbool.h>
#include "Driver_USART.h"
#include "main.h"


/* .................................Theread Function...................................*/

void UART_receive(void *argument);
void uart_send_thread(void* arg);
void Consol_Task (void *argument); 
void RX_Verify (void *argument); 
void Keypad_Task (void *argument);                                 

/* .................................Theread Initilization...................................*/

int init_uart_receive_thread(	ARM_DRIVER_USART *pUsart	);
int init_uart_send_thread(	ARM_DRIVER_USART *pUsart);
int	  Init_Consol_Task (void);
int 	Init_RX_Verify (void);
int 	Init_Keypad_Task(void);
int 	Init_USB_Task(void);




/* .................................Theread Priority...................................*/

#define CONSOL_TASK_PRIORITY    osPriorityNormal
#define RX_VERIFY_PRIORITY      osPriorityNormal
#define MY_UART_PRIORITY        osPriorityNormal
#define KEYPAD_PRIORITY        osPriorityNormal1
#define USB_TASK_PRIORITY        osPriorityNormal1

/* .................................Thread Flags...................................*/
#define FLAG_UART_RECEIVE   					0x01 
#define FLAG_UART_RX_VERIFY						0x02
#define FLAG_CONSOLE_TASK							0x03
#define FLAG_KEYPAD_TASK							0x04
#define FLAG_UART_SEND								0x05

/* .................................Size of Queue...................................*/

#define CONSOL_QUEUE_SIZE   			2
#define MSGQUEUE_OBJECTS      		16                                 // number of Message Queue Objects
#define UART_IN_QUEUE_SIZE      	16  
#define KEYPAD_OUT_SIZE   				16
#define USB_QUEUE_SIZE   					16



#define MAX_PAYLOAD_SIZE_CONSOLE_IN 		50

	typedef struct {                                                   // object data type
			char 			direction[2];
			char  		Cmd_hi;
			char    	Cmd_lo;
			char    	ascii_seperator;
			char    	payload[MAX_PAYLOAD_SIZE_CONSOLE_IN];  																		//payload has to be in multiple of 4 
			
	} CONSOL_TASK_STRU_1;
	
	
	typedef struct {	
			char response_type;
			char cmd_hi;
			char cmd_lo;
			char payload_type;
			uint16_t payload_size; 
			
		}WRAPPER_INPUT;
	


//CONSOL_TASK_STRU_1 WrapConsole_data(char* keypad_stru_wrapper , WRAPPER_INPUT wrapper_data )	;



#endif  /* THREAD_PARAM_H */

