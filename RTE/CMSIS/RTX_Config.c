/*
 * Copyright (c) 2013-2017 ARM Limited. All rights reserved.
 *
 * SPDX-License-Identifier: Apache-2.0
 *
 * Licensed under the Apache License, Version 2.0 (the License); you may
 * not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an AS IS BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * -----------------------------------------------------------------------------
 *
 * $Revision:   V5.1.0
 *
 * Project:     CMSIS-RTOS RTX
 * Title:       RTX Configuration
 *
 * -----------------------------------------------------------------------------
 */
 
#include "cmsis_compiler.h"
#include "rtx_os.h"
#include "..\source\main.h"


 void Delay_ms(uint32_t ms_delay);
 void Usage_per(uint32_t diff_systick_idle);
 void send_usage_to_console(float usage);
 
extern uint32_t SystemCoreClock;

	float fl_usage_per;
#define BUFFER_SIZE   100
#define DELAY_TIME_IDEAL  1
#define USAGE_DATA_TO_CONSOLE_INTERVAL_MS   1000
//#define COMMOND_USAGE  						10                  // need to confirm it from online sheet
#define COMMOND_SOURCE_IDEAL      'I'

typedef struct
{
  uint8_t m_ucBlkIndex;
  uint16_t m_uiBlkFinalVal;
  uint32_t m_ulBlkAvgAccVal;
}BLOCK_BUFF;

typedef union 
{
	float fl_calue_per;
	char  ch_calue_per[4];
	
}U_FLOAT;

typedef struct 
{
	float cpu_usage;
	uint32_t current_systick;
	
}IDEAL_USAGE;

uint16_t task_section =0;
//#define IDEAL_COMMOND_HI  51
//#define IDEAL_COMMOND_LOW  'I'
//typedef struct _BlockAvgBuf BlockAvgBuf;


void block_average( BLOCK_BUFF *pBlockAvgBuf, uint32_t uiMovAvgVal, uint16_t ucBufferSize );

// OS Idle Thread
__WEAK __NO_RETURN void osRtxIdleThread (void *argument) {
  (void)argument;
	static uint32_t ms_delay ;
	uint32_t u32_old_systick_idle,u32_curr_systick_idle,u32_diff_systick_idle;
	static uint32_t u32_send_data_tick = 0;
	BLOCK_BUFF s_accumulation;
	WRAPPER_INPUT wrapper_data_ideal;
	char buffer_data[8];
	CONSOL_TASK_STRU_1 idle_console_in;
	osStatus_t q_status_console_ideal ;
	IDEAL_USAGE s_usage;
	
	wrapper_data_ideal.response_type = INPUT_RESPONSE;
	
	wrapper_data_ideal.cmd_hi  = COMMOND_CON_IDEAL;
	wrapper_data_ideal.cmd_lo  = IDEAL_COMMOND_LOW;
	
	wrapper_data_ideal.payload_type = PAYLOAD_BASE64;
	
	wrapper_data_ideal.payload_size = sizeof(s_usage);
	
	
  for (;;) {
	 // setpin 1.5
		task_section = 1;
		u32_old_systick_idle = osKernelGetTickCount();   										// save initial systick 
		
		Delay_ms (DELAY_TIME_IDEAL);																				// create hard delay of 1 ms 
		
		u32_curr_systick_idle = osKernelGetTickCount();											// check systic after complication delay 
			
		u32_diff_systick_idle = u32_curr_systick_idle - u32_old_systick_idle; // check the difference 
		

		task_section =2;
						block_average( &s_accumulation, u32_diff_systick_idle , BUFFER_SIZE ); // check block average 
		task_section = 3;
//		fl_usage_per = (100 - (DELAY_TIME_IDEAL*1/ accumulation.m_uiBlkFinalVal) *100);
		fl_usage_per = (100 -  ((100/ s_accumulation.m_uiBlkFinalVal)*DELAY_TIME_IDEAL)); // calcute usage buffer 
		task_section = 4;

		
		if(u32_send_data_tick < u32_curr_systick_idle)  //logic to send usage data at regular interval 
		{
//			send_usage_to_console(fl_usage_per);
			
				s_usage.cpu_usage = fl_usage_per;
				s_usage.current_systick = osKernelGetTickCount();
			  memcpy(buffer_data ,&s_usage ,sizeof(s_usage));
			
				osMutexAcquire(mutex_is_WrapConsole_data, osWaitForever);
				WrapConsole_data(buffer_data,wrapper_data_ideal , &idle_console_in);
				osMutexRelease(mutex_is_WrapConsole_data);
			
				q_status_console_ideal = osMessageQueuePut (Q_Consol_Task_IN ,&idle_console_in ,0,NULL);
				
						
			  u32_send_data_tick = u32_curr_systick_idle + USAGE_DATA_TO_CONSOLE_INTERVAL_MS ;
		}
		task_section = 5;
//		if(u32_send_data_tick < (u32_send_data_tick + USAGE_DATA_TO_CONSOLE_INTERVAL_MS))  //to handle overflow of systick 
//		{
//			u32_send_data_tick = 0;
//		}
		//clear 1.5

	}
}
 
osThreadId_t thread_id;

// OS Error Callback function
__WEAK uint32_t osRtxErrorNotify (uint32_t code, void *object_id) {
  (void)object_id;

  switch (code) {
    case osRtxErrorStackUnderflow:
      // Stack underflow detected for thread (thread_id=object_id)
			thread_id=object_id ;
      break;
    case osRtxErrorISRQueueOverflow:
      // ISR Queue overflow detected when inserting object (object_id)
      break;
    case osRtxErrorTimerQueueOverflow:
      // User Timer Callback Queue overflow detected for timer (timer_id=object_id)
      break;
    case osRtxErrorClibSpace:
      // Standard C/C++ library libspace not available: increase OS_THREAD_LIBSPACE_NUM
      break;
    case osRtxErrorClibMutex:
      // Standard C/C++ library mutex initialization failed
      break;
    default:
      break;
  }
  for (;;) {}
//return 0U;
}


void Delay_ms(uint32_t ms_delay)
{
	
		ms_delay *= (SystemCoreClock/10000);
	  while (ms_delay--) { __nop(); __nop(); __nop(); __nop(); __nop(); __nop(); }
}



void block_average( BLOCK_BUFF *pBlockAvgBuf, uint32_t uiMovAvgVal, uint16_t ucBufferSize )
{
  // Add value into the block average buffer
  pBlockAvgBuf->m_ulBlkAvgAccVal += uiMovAvgVal;
  // Increment index value
  pBlockAvgBuf->m_ucBlkIndex++;
  // Check if index reached to max block buffer size
  if( pBlockAvgBuf->m_ucBlkIndex >= ucBufferSize )
  {
    // Copy average value into final value of the structure
  	pBlockAvgBuf->m_uiBlkFinalVal = ( uint16_t )( pBlockAvgBuf->m_ulBlkAvgAccVal / ucBufferSize ); 
    // Clear block average accumulated value
    pBlockAvgBuf->m_ulBlkAvgAccVal = 0;
    // Clear block average index 
    pBlockAvgBuf->m_ucBlkIndex = 0;
  }
	
}
